package org.craft11;

import org.bukkit.event.Event;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Bedhead extends JavaPlugin {
    Listener thisListener = new Listener();

    public static Bedhead Instance;

    @Override
    public void onDisable() {
        this.getServer().getScheduler().cancelTasks(this);
    }

    @Override
    public void onEnable() {
        Instance = this;
        RegisterEvents();
    }

    private void RegisterEvents() {
        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvent(Event.Type.PLAYER_BED_ENTER, thisListener, Event.Priority.Normal, this);
        pm.registerEvent(Event.Type.PLAYER_BED_LEAVE, thisListener, Event.Priority.Normal, this);
    }
}
