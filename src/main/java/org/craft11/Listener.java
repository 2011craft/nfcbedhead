package org.craft11;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.material.Bed;

import java.util.HashMap;

public class Listener extends PlayerListener {
    HashMap<World, Integer> sleepingPlayers = new HashMap<>();
    HashMap<World, Boolean> skippingTheNight = new HashMap<>();
    HashMap<World, Integer> runningTasks = new HashMap<>();

    @Override
    public void onPlayerBedEnter(PlayerBedEnterEvent event) {
        processSleepingPlayersUpdate(event.getPlayer(), 1);
    }

    @Override
    public void onPlayerBedLeave(PlayerBedLeaveEvent event) {
        processSleepingPlayersUpdate(event.getPlayer(), -1);
    }

    private void processSleepingPlayersUpdate(Player player, int diff) {
        World w = player.getWorld();
        boolean isWorldSkippingNight = skippingTheNight.get(w);
        int worldTask = runningTasks.getOrDefault(w, -1);

        int current = sleepingPlayers.getOrDefault(w, 0);
        current += diff;
        sleepingPlayers.put(w, current);

        int needed = Math.max(1, (int) Math.floor(0.33 * player.getWorld().getPlayers().size()));

        if (!isWorldSkippingNight)
            broadcastToWorld(w, "There are currently " + current + "/" + needed + " needed players in bed.");

        if (current >= needed) {
            skippingTheNight.put(w, true);
            int task = Bedhead.Instance.getServer().getScheduler().scheduleAsyncDelayedTask(
                    Bedhead.Instance,
                    new SkipNightRunnable(w),
                    125L
            );
            runningTasks.put(w, task);
        } else if (worldTask != -1) {
            Bedhead.Instance.getServer().getScheduler().cancelTask(worldTask);
            runningTasks.remove(w);
        }
    }

    private void broadcastToWorld(World w, String message) {
        for (Player p : w.getPlayers()) {
            p.sendMessage(message);
        }
    }

    private class SkipNightRunnable implements Runnable {
        private final World w;

        private SkipNightRunnable(World w){
            this.w = w;
        }

        @Override
        public void run() {
            broadcastToWorld(w, "Skipping the night...");
            long ticksUntilMorning = 24_000L - (w.getFullTime() % 24_000L);
            long ticksTarget = w.getFullTime() + ticksUntilMorning;
            w.setFullTime(ticksTarget);
            Bedhead.Instance.getServer().getScheduler().scheduleAsyncDelayedTask(
                    Bedhead.Instance,
                    () -> {
                        skippingTheNight.remove(w);
                        sleepingPlayers.remove(w);
                        runningTasks.remove(w);
                    },
                    50L
            );
        }
    }
}
